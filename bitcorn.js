// ==UserScript==
// @name     Bitcorn Battle
// @version  0.1
// @grant    none
// @include	https://bitcorns.com/farms/*
// @description add weather to Bitcorn farms, current weather from openweathermap
// @attribution fancy weather animations from Josh Bader
// @repo git clone git@bitbucket.org:bocamike/bitcornweather.git
// ==/UserScript==

// grab BTC address, use it for API call to bitcorns,
// use lat, long for API call to locationiq.org to get city + country, call weather API
// @require https://code.jquery.com/jquery-3.3.1.min.js - nah!
// @resource http://bitcornbattle.com/css/weather.css

// must be on farm detail page and have location set
var isFarmPage = window.location.pathname.match('/farms/');
if(isFarmPage && typeof document.getElementsByTagName('small')[0] !== undefined &&
	document.getElementsByTagName('small')[0].firstChild.textContent !== undefined) 
	var where = document.getElementsByTagName('small')[0].firstChild.textContent.split(/,\s*/);
else return true;

var url = window.location.href,
	btcaddress  = url.substr(url.lastIndexOf('/') + 1),
	city, state, country, temp, weatherdesc, weathershort, weatherid, weathericon, humidity, sunrise, sunset, forecast, date, divtitle,
	farmname = document.getElementById('profile').firstChild.textContent,
	//oldfarmname = $("#profile>h5").text(),
	geourl = 'https://us1.locationiq.org/v1/reverse.php?key=94fca6fd64efa4&lat=' + where[0] + '&lon=' + where[1] + '&format=json',
	weatherkey = '1787f396e5690a621196deaf9b21a8eb',
	weatherurl = 'http://api.openweathermap.org/data/2.5/weather?lat=' + where[0] + '&lon=' + where[1] + '&APPID=' + weatherkey + '&units=metric',
	wcss  = ".icon { position:relative; top:134px; left:86px; display:inline; z-index: 12345; width:12em; height:10em; font-size:1em; /* control icon size here */} .cloud {position: absolute; z-index: 1; top: 50%; left: 50%; width: 3.6875em; height: 3.6875em; margin: -1.84375em; background: currentColor; border-radius: 50%; box-shadow: -2.1875em 0.6875em 0 -0.6875em, 2.0625em 0.9375em 0 -0.9375em, 0 0 0 0.375em #fff, -2.1875em 0.6875em 0 -0.3125em #fff, 2.0625em 0.9375em 0 -0.5625em #fff;}.cloud:after { content: ''; position: absolute; bottom: 0; left: -0.5em; display: block; width: 4.5625em; height: 1em; background: currentColor; box-shadow: 0 0.4375em 0 -0.0625em #fff;}.cloud:nth-child(2) { z-index: 0; background: #fff; box-shadow: -2.1875em 0.6875em 0 -0.6875em #fff, 2.0625em 0.9375em 0 -0.9375em #fff, 0 0 0 0.375em #fff, -2.1875em 0.6875em 0 -0.3125em #fff, 2.0625em 0.9375em 0 -0.5625em #fff; opacity: 0.3; transform: scale(0.5) translate(6em, -3em); animation: cloud 4s linear infinite;}.cloud:nth-child(2):after { background: #fff; }.sun { position: absolute; top: 50%; left: 50%; width: 2.5em; height: 2.5em; margin: -1.25em; background: currentColor; border-radius: 50%; box-shadow: 0 0 0 0.375em #fff; animation: spin 12s infinite linear;}.rays { position: absolute; top: -2em; left: 50%; display: block; width: 0.375em; height: 1.125em; margin-left: -0.1875em; background: #fff; border-radius: 0.25em; box-shadow: 0 5.375em #fff;}.rays:before,.rays:after { content: ''; position: absolute; top: 0em; left: 0em; display: block; width: 0.375em; height: 1.125em; transform: rotate(60deg); transform-origin: 50% 3.25em; background: #fff; border-radius: 0.25em; box-shadow: 0 5.375em #fff;}.rays:before { transform: rotate(120deg);}.cloud + .sun { margin: -2em 1em;}.rain,.lightning,.snow { position: absolute; z-index: 2; top: 50%; left: 50%; width: 3.75em; height: 3.75em; margin: 0.375em 0 0 -2em; background: currentColor;}.rain:after { content: ''; position: absolute; z-index: 2; top: 50%; left: 50%; width: 1.125em; height: 1.125em; margin: -1em 0 0 -0.25em; background: #0cf; border-radius: 100% 0 60% 50% / 60% 0 100% 50%; box-shadow: 0.625em 0.875em 0 -0.125em rgba(255,255,255,0.2), -0.875em 1.125em 0 -0.125em rgba(255,255,255,0.2), -1.375em -0.125em 0 rgba(255,255,255,0.2); transform: rotate(-28deg); animation: rain 3s linear infinite;}.bolt { position: absolute; top: 50%; left: 50%; margin: -0.25em 0 0 -0.125em; color: #fff; opacity: 0.3; animation: lightning 2s linear infinite;}.bolt:nth-child(2) { width: 0.5em; height: 0.25em; margin: -1.75em 0 0 -1.875em; transform: translate(2.5em, 2.25em); opacity: 0.2; animation: lightning 1.5s linear infinite;}.bolt:before,.bolt:after { content: ''; position: absolute; z-index: 2; top: 50%; left: 50%; margin: -1.625em 0 0 -1.0125em; border-top: 1.25em solid transparent; border-right: 0.75em solid; border-bottom: 0.75em solid; border-left: 0.5em solid transparent; transform: skewX(-10deg);}.bolt:after { margin: -0.25em 0 0 -0.25em; border-top: 0.75em solid; border-right: 0.5em solid transparent; border-bottom: 1.25em solid transparent; border-left: 0.75em solid; transform: skewX(-10deg);}.bolt:nth-child(2):before { margin: -0.75em 0 0 -0.5em; border-top: 0.625em solid transparent; border-right: 0.375em solid; border-bottom: 0.375em solid; border-left: 0.25em solid transparent;}.bolt:nth-child(2):after { margin: -0.125em 0 0 -0.125em; border-top: 0.375em solid; border-right: 0.25em solid transparent; border-bottom: 0.625em solid transparent; border-left: 0.375em solid;}.flake:before,.flake:after { content: '\2744'; position: absolute; top: 50%; left: 50%; margin: -1.025em 0 0 -1.0125em; color: #fff; list-height: 1em; opacity: 0.2; animation: spin 8s linear infinite reverse;}.flake:after { margin: 0.125em 0 0 -1em; font-size: 1.5em; opacity: 0.4; animation: spin 14s linear infinite;}.flake:nth-child(2):before { margin: -0.5em 0 0 0.25em; font-size: 1.25em; opacity: 0.2; animation: spin 10s linear infinite;}.flake:nth-child(2):after { margin: 0.375em 0 0 0.125em; font-size: 2em; opacity: 0.4; animation: spin 16s linear infinite reverse;}@keyframes spin { 100% { transform: rotate(360deg); }}@keyframes cloud { 0% { opacity: 0; } 50% { opacity: 0.3; } 100% { opacity: 0; transform: scale(0.5) translate(-200%, -3em); }}@keyframes rain { 0% { background: #0cf; box-shadow: 0.625em 0.875em 0 -0.125em rgba(255,255,255,0.2),  -0.875em 1.125em 0 -0.125em rgba(255,255,255,0.2), -1.375em -0.125em 0 #0cf; } 25% { box-shadow: 0.625em 0.875em 0 -0.125em rgba(255,255,255,0.2), -0.875em 1.125em 0 -0.125em #0cf, -1.375em -0.125em 0 rgba(255,255,255,0.2); } 50% { background: rgba(255,255,255,0.3); box-shadow: 0.625em 0.875em 0 -0.125em #0cf, -0.875em 1.125em 0 -0.125em rgba(255,255,255,0.2), -1.375em -0.125em 0 rgba(255,255,255,0.2); } 100% { box-shadow: 0.625em 0.875em 0 -0.125em rgba(255,255,255,0.2), -0.875em 1.125em 0 -0.125em rgba(255,255,255,0.2), -1.375em -0.125em 0 #0cf; }} @keyframes lightning { 45% { color: #fff; background: #fff; opacity: 0.2; } 50% { color: #0cf; background: #0cf; opacity: 1; } 55% { color: #fff; background: #fff; opacity: 0.2; }}";


function injectCSS() {
	var s = document.createElement("style");
	s.innerHTML = wcss;
	document.body.insertBefore(s, document.body.firstChild);
}

function setSunny() {
	var sunnydiv = document.createElement('div');
	sunnydiv.className = "icon sunny";
	sunnydiv.setAttribute('title', forecast);
	sunnydiv.innerHTML = '<div class="sun"><div class="rays"></div></div>';
	document.body.insertBefore(sunnydiv, document.body.firstChild);
}

function setRainy() {
	var rainydiv = document.createElement('div');
	rainydiv.className = "icon rainy";
	rainydiv.setAttribute('title', forecast);
	rainydiv.innerHTML = '<div class="cloud"></div><div class="rain"></div>';
	document.body.insertBefore(rainydiv, document.body.firstChild);
}

function setSnowy() {
	var snowydiv = document.createElement('div');
	snowydiv.className = "icon flurries";
	snowydiv.setAttribute('title', forecast);
	snowydiv.innerHTML = '<div class="cloud"></div><div class="snow"><div class="flake"></div><div class="flake"></div></div>';
	document.body.insertBefore(snowydiv, document.body.firstChild);
}

function setCloudy() {
	var cloudydiv = document.createElement('div');
	cloudydiv.className = "icon cloudy";
	cloudydiv.setAttribute('title', forecast);
	cloudydiv.innerHTML = '<div class="cloud"></div><div class="cloud"></div>';
	document.body.insertBefore(cloudydiv, document.body.firstChild);
}

function setPartlySunny() {
	var partlysunnydiv = document.createElement('div');
	partlysunnydiv.className = "icon sun-shower";
	partlysunnydiv.setAttribute('title', forecast);
	partlysunnydiv.innerHTML = '<div class="cloud"></div><div class="sun"><div class="rays"></div></div><div class="rain"></div>';
	document.body.insertBefore(partlysunnydiv, document.body.firstChild);
}

function setStormy() {
	var stormydiv = document.createElement('div');
	stormydiv.className = "icon thunder-storm";
	stormydiv.setAttribute('title', forecast);
	stormydiv.innerHTML = '<div class="cloud"></div><div class="lightning"><div class="bolt"></div><div class="bolt"></div></div>';
	document.body.insertBefore(stormydiv, document.body.firstChild);
}



if(isFarmPage) {
	injectCSS();

	fetch(geourl)
	.then(response => response.json())
	.then(data => {
	    city = typeof data.address['city'] !== 'undefined' ? data.address['city'] : '';
	    state = typeof data.address['state'] !== 'undefined' ? data.address['state'] : '';
	    country = data.address['country'];
		//alert(farmname + ' (' + btcaddress + ') is at ' + lat + ', ' + long + ' (' + city + ', ' + country + ')');

		fetch(weatherurl)
		.then(response => response.json())
		.then(data => {
			weatherdesc = data.weather[0]['description'];
			weathershort = data.weather[0]['main'];
			weatherid = data.weather[0]['icon'];
			temp = data.main['temp'] + 'C';
			humidity = data.main['humidity'];

			date = new Date(data.sys['sunrise'] * 1000);
			sunrise = '' + date.getHours() + ':' + ('0' + date.getMinutes()).substr(-2) + ':' + ("0" + date.getSeconds()).substr(-2) + ' UTC';
			date = new Date(data.sys['sunset'] * 1000);
			sunset = '' + date.getHours() + ':' + ('0' + date.getMinutes()).substr(-2) + ':' + ("0" + date.getSeconds()).substr(-2) + ' UTC';

			forecast  = 'Forecast for ' + farmname + ' (';
			forecast += city.length > 0 ? city + ', ' : '';
			forecast += state.length > 0 ? state + ', ' : '';
			forecast += country + ")\n \n";
			forecast += weatherdesc.charAt(0).toUpperCase() + weatherdesc.slice(1) + "\nCurrent temp: " + temp;
			forecast += "\nHumidity: " + humidity + "%\nSunrise: " +sunrise+ ', Sunset: ' + sunset + "\n";

			// thunderstorm, drizzle, rain, snow, atmosphere, clear, clouds
			if(weathershort === "Clear" || weathershort === 'Haze')				setSunny();
			else if(weathershort === 'Clouds' || weathershort === 'Fog')		setCloudy();
			else if(weathershort === "Clear" || weathershort === 'Clouds')		setSunny();
			else if(weathershort === "Rain" || weathershort === 'Drizzle' || weathershort==='Thunderstorm')
																				setRainy();
			else if(weathershort === "Snow")									setSnowy();
			else if(weathershort === "Mist")									setPartlySunny();
			else console.log(forecast + ' ' + weathershort);
	      
		})
		.catch(function() {
			alert("there was a super funky problem, damn!");
		});
	})
	.catch(function() {
	    alert("there was a really funky problem, damn!");
	});
  

}

